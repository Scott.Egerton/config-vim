" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU Affero General Public License as published by
" the Free Software Foundation, either version 3 of the License, or
" (at your option) any later version.

" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU Affero General Public License for more details.

" You should have received a copy of the GNU Affero General Public License
" along with this program.  If not, see <http://www.gnu.org/licenses/>.

" Enable Mouse mode
set mouse=a

" Show line numbers
set nu

" Highlight current line
set cursorline

" Use spaces not tabs
filetype plugin indent on

" show existing tab with 2 spaces width
set tabstop=2

" when indenting with '>', use 2 spaces width
set shiftwidth=2

" On pressing tab, insert 2 spaces
set expandtab

" Auto reload the file when it changes externally
set autoread

" Use system clipboard
set clipboard^=unnamed,unnamedplus

" Stops formatting messing up when pasting from outside vim
set paste

" Change default spelling Highlight colour
"highlight SpellBad ctermbg=yellow
"highlight SpellBad ctermbg=red ctermfg=white
highlight SpellBad ctermbg=white ctermfg=black
" Allow lines to start with lowercase letters (Useful for code comments)
set spellcapcheck=<CR>

" When pasting, don't yank selected text
" https://superuser.com/a/321726
vnoremap p "_dP
vnoremap P "_dP

" Make :W the same as :w
cnoreabbrev W w

set ignorecase

" When using the built in terminal :term don't spell check
au TermOpen * setlocal nospell
" When using the built in terminal :term don't show line numbers
au TermOpen * setlocal nonu

autocmd BufEnter * match Todo "@todo"
"autocmd InsertEnter * match Todo "@todo"

" MuComplete
let g:mucomplete#enable_auto_at_startup = 1
set completeopt+=menuone
set completeopt+=noinsert
" Don't auto open preview window
set completeopt-=preview

" TTags
" Show available tags with \g.
noremap <Leader>g. :TTags<cr>
" Show current buffer's tags with \g%
noremap <Leader>g% :call ttags#List(0, "*", "", @%)<cr>

" Lightline
" Removes file format (unix) file encoding (utf8), and file type from right hand
" side of statusline.
let g:lightline = {
      \ 'active': {
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'linter_errors',
      \                'linter_warnings'] ]
      \ }
      \ }

" Intergrate lightline and ale
let g:lightline.component_expand = {
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors'
      \ }

" Intergrate lightline and ale
let g:lightline.component_type = {
      \     'linter_warnings': 'warning',
      \     'linter_errors': 'error',
      \ }

" Removes mode e.g. '--INSERT--' as it is duplicate information when using
" lightline
set noshowmode

" Ale
let g:ale_fixers = {
\   'cpp': [ 'remove_trailing_lines',
\            'trim_whitespace',
\            'uncrustify'],
\   'c':   [ 'remove_trailing_lines',
\            'trim_whitespace',
\            'uncrustify',
\            'clang-format' ],
\   'td': [  'remove_trailing_lines',
\            'trim_whitespace'],
\  'text': [ 'remove_trailing_lines',
\            'textlint',
\            'trim_whitespace']
\}

let g:ale_languagetool_executable = 'languagetool-commandline'

let g:ale_linters = {
\  'text': [ 'alex',
\            'languagetool',
\            'proselint',
\            'redpen',
\            'textlint',
\            'vale',
\            'write-good']
\}

let g:ale_fix_on_save = 1

" Needed to recognise C++17. -Weverything/all/extra is here just in case.
" Also disable warnings about compatibility with old versions of C++
let g:ale_cpp_clang_options =  '-std=c++1z
                              \ -Weverything
                              \ -Wno-c++98-compat
                              \ -Wno-c++11-compat
                              \ -Wno-c++14-compat
                              \ -Wno-c++98-c++11-compat-binary-literal '
let g:ale_cpp_clangd_options = '-std=c++1z
                              \ -Weverything'
let g:ale_cpp_gcc_options =    '-std=c++1z
                              \ -Wall
                              \ -Wextra'

" Extra warnings
let g:ale_cpp_cppcheck_options = '--enable=all'

" Enable all clangtidy checks except these few
let g:ale_cpp_clangtidy_checks = [
    \ '*',
    \ '-fuchsia-default-arguments',
    \ '-google-readability-todo'
    \]

let g:ale_cpp_cpplint_options = '+* -fuchsia-default-arguments -google-readability-todo'

" Gutentags
let g:gutentags_ctags_exclude = ['*.json']

" Use deoplete.
let g:deoplete#enable_at_startup = 1

" Auto install Vim-plug
" See https://github.com/junegunn/vim-plug/issues/739#issuecomment-516953621
let plug_install = 0
let autoload_plug_path = stdpath('config') . '/autoload/plug.vim'
if !filereadable(autoload_plug_path)
    silent exe '!curl -fL --create-dirs -o ' . autoload_plug_path .
        \ ' https://raw.github.com/junegunn/vim-plug/master/plug.vim'
    execute 'source ' . fnameescape(autoload_plug_path)
    let plug_install = 1
endif
unlet autoload_plug_path

" Vim-plug
call plug#begin("$XDG_CONFIG_HOME/nvim/plugins")

Plug 'tpope/vim-sensible' " Sensible defaults

Plug 'w0rp/ale' " Multiple static analysers

Plug 'rhysd/vim-clang-format' " Clang format selection support

" Autocomplete plugins
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'ludovicchabant/vim-gutentags' " Adds CTags for autocomplete
                                    " Requires universal-ctags or other ctags
                                    " generator
"Plug 'tomtom/ttags_vim' " Tags browser
"Plug 'tomtom/tlib_vim' " Needed for ttags
Plug 'majutsushi/tagbar'

" Git integration
Plug 'tpope/vim-fugitive'

Plug 'mhinz/vim-signify' " Adds git line status in left gutter

Plug 'itchyny/lightline.vim' " Lightweight status line
Plug 'maximbaz/lightline-ale' " Lightline Ale integration

Plug 'terryma/vim-multiple-cursors' " Multple cursors with Ctrl-n

Plug 'scrooloose/nerdcommenter' " Keybindings to assist with commenting
                                " See :help nerdcommenter

Plug 'ntpeters/vim-better-whitespace' " Highlight trailing whitespace

Plug 'rakr/vim-one' " Atom one colorscheme

" Initialize plugin system
call plug#end()

" If Vim-Plug has just been installed, install all plugins
" See https://github.com/junegunn/vim-plug/issues/739#issuecomment-516953621
if plug_install
    PlugInstall --sync
endif
unlet plug_install

" Use vim-one colours
colorscheme one

" Use default terminal colours
"colorscheme default
"set t_Co=16

" Highlight 80 column limit
set colorcolumn=80
highlight ColorColumn ctermbg=RED cterm=NONE guibg=RED gui=NONE

" Signify
highlight SignColumn ctermbg=NONE cterm=NONE guibg=NONE gui=NONE
highlight DiffAdd    ctermbg=NONE cterm=bold ctermfg=green
highlight DiffChange ctermbg=NONE cterm=bold ctermfg=yellow
highlight DiffDelete ctermbg=NONE cterm=bold ctermfg=red
